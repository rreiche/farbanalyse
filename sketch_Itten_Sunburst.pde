// Ruth Reiche, 2018
// Das Skript erzeugt ein Sunburst-Diagramm aus einem auf die 12 von Johannes Itten benannten Grundfarben reduzierten Bild. 
// Um ein Chord-Diagramm zu erhalten, wie in meinem Aufsatz "Digitale Ikonik" abgebildet, müssen Verbindungslinien im Anschluss manuell gezogen werden.
// Es ist eine Vorverarbeitung notwendig. Mit jedem gängigen Bildbearbeitungsprogramm lassen sich eigene Paletten erstellen. Für die RGB-Werte siehe unten color[] itten.


import generativedesign.*;
import java.util.Calendar;

PImage img;
color[] colors;
String bildname;

color[] itten ={
color(244,229,0), //gelb
color(253,198,11), //dunkelgelb
color(241,142,28), //orange
color(234,98,31), //orangerot
color(227,35,34), //rot
color(196,3,125), //purpurrot
color(109,57,139), //violett
color(68,78,153), //blauviolett
color(42,113,176), //blau
color(6,150,187), //blaugrün
color(0,142,91), //grün
color(140,187,38) //hellgrün
};



void setup(){ 
  
  size (600,600); //Leinwandgröße für das Kuchendiagramm
  background(255); //Hintergrundfarbe
  noStroke();
  noLoop();
  
  img = loadImage("Dateiname.png"); //Bild laden - Dateiname muss exakt übereinstimmen!
  bildname ="Dateiname"; // unter diesem Namen wird das generierte Bild gespeichert
  
}


void draw(){

// Farben aus dem Bild auslesen
int j = 0; 
colors = new color[img.width*img.height];
for (int gridY=0; gridY<img.height; gridY++) {
  for (int gridX=0; gridX<img.width; gridX++) {
    colors[j] = img.get(gridX, gridY);
    j++;
  }
}

//Farbanteile berechnen
int zaehler_farbe1=0; //gelb
int zaehler_farbe2=0; //dunkelgelb
int zaehler_farbe3=0; //orange
int zaehler_farbe4=0; //orangerot
int zaehler_farbe5=0; //rot
int zaehler_farbe6=0; //purpurrot
int zaehler_farbe7=0; //violett
int zaehler_farbe8=0; //blauviolett
int zaehler_farbe9=0; //blau
int zaehler_farbe10=0; //blaugrün
int zaehler_farbe11=0; //grün
int zaehler_farbe12=0; //hellgrün

int k=0;
while(k<colors.length){
if (colors[k]==itten[0]){zaehler_farbe1++;}
if (colors[k]==itten[1]){zaehler_farbe2++;}
if (colors[k]==itten[2]){zaehler_farbe3++;}
if (colors[k]==itten[3]){zaehler_farbe4++;}
if (colors[k]==itten[4]){zaehler_farbe5++;}
if (colors[k]==itten[5]){zaehler_farbe6++;}
if (colors[k]==itten[6]){zaehler_farbe7++;}
if (colors[k]==itten[7]){zaehler_farbe8++;}
if (colors[k]==itten[8]){zaehler_farbe9++;}
if (colors[k]==itten[9]){zaehler_farbe10++;}
if (colors[k]==itten[10]){zaehler_farbe11++;}
if (colors[k]==itten[11]){zaehler_farbe12++;}
k++;
}

//Ausgabe in der Konsole (als Test)
println("Farbe 1 - gelb: "+zaehler_farbe1);
println("Farbe 2 - dunkelgelb: "+zaehler_farbe2);
println("Farbe 3 - orange: "+zaehler_farbe3);
println("Farbe 4 - orangerot: "+zaehler_farbe4);
println("Farbe 5 - rot: "+zaehler_farbe5);
println("Farbe 6 - purpurrot: "+zaehler_farbe6);
println("Farbe 7 - violett: "+zaehler_farbe7);
println("Farbe 8 - blauviolett: "+zaehler_farbe8);
println("Farbe 9 - blau: "+zaehler_farbe9);
println("Farbe 10 - blaugrün: "+zaehler_farbe10);
println("Farbe 11 - grün: "+zaehler_farbe11);
println("Farbe 12 - hellgrün: "+zaehler_farbe12);


//Prozentualen Anteil berechnen
float pixelanzahl=img.width*img.height;

float prozent_farbe1=zaehler_farbe1*100/pixelanzahl; //gelb
float prozent_farbe2=zaehler_farbe2*100/pixelanzahl; //dunkelgelb
float prozent_farbe3=zaehler_farbe3*100/pixelanzahl; //orange
float prozent_farbe4=zaehler_farbe4*100/pixelanzahl; //orangerot
float prozent_farbe5=zaehler_farbe5*100/pixelanzahl; //rot
float prozent_farbe6=zaehler_farbe6*100/pixelanzahl; //purpurrot
float prozent_farbe7=zaehler_farbe7*100/pixelanzahl; //violett
float prozent_farbe8=zaehler_farbe8*100/pixelanzahl; //blauviolett
float prozent_farbe9=zaehler_farbe9*100/pixelanzahl; //blau
float prozent_farbe10=zaehler_farbe10*100/pixelanzahl; //blaugrün
float prozent_farbe11=zaehler_farbe11*100/pixelanzahl; //grün
float prozent_farbe12=zaehler_farbe12*100/pixelanzahl; //hellgrün


//Ausgabe in der Konsole (als Test)
println("Prozent Farbe 1 - gelb: "+prozent_farbe1);
println("Prozent Farbe 2 - dunkelgelb: "+prozent_farbe2);
println("Prozent Farbe 3 - orange: "+prozent_farbe3);
println("Prozent Farbe 4 - orangerot: "+prozent_farbe4);
println("Prozent Farbe 5 - rot: "+prozent_farbe5);
println("Prozent Farbe 6 - purpurrot: "+prozent_farbe6);
println("Prozent Farbe 7 - violett: "+prozent_farbe7);
println("Prozent Farbe 8 - blauviolett: "+prozent_farbe8);
println("Prozent Farbe 9 - blauviolett: "+prozent_farbe9);
println("Prozent Farbe 10 - blaugrün: "+prozent_farbe10);
println("Prozent Farbe 11 - grün: "+prozent_farbe11);
println("Prozent Farbe 12 - hellgrün: "+prozent_farbe12);

//zeichne Kreis nach Itten

/*Parameter für Kreise: 
Mittelpunkt-x-Koordinate, 
Mittelpunkt-y-Koordinate, 
Durchmesser Breite, 
Durchmesser Höhe
start-Winkel,
End-Winkel, 
Modus
*/

int i=0;
int x = 300;
int y = 300;
int breite = 300;
int hoehe = 300;
int start =255; //für Sunburst
int ende = 285; //für Sunburst

//Prozentangaben
float[] prozentangaben= {prozent_farbe1,prozent_farbe2,prozent_farbe3,prozent_farbe4,prozent_farbe5,prozent_farbe6,prozent_farbe7,prozent_farbe8,prozent_farbe9,prozent_farbe10,prozent_farbe11,prozent_farbe12};


while (i<12){
  fill(itten[i]);
  
  //Kuchendiagramm mit Prozentangeben, Sunburst-Charakter (Durchmesser ändern)
  arc(x,y,breite+prozentangaben[i]*3,hoehe+prozentangaben[i]*3,radians(start),radians(ende),PIE);
  i++;
  start =start+30;
  ende=ende+30;

}

//zeichne weißen Kreis, damit ein Ring entsteht
fill(255,255,255);
arc(x,y,301,301,radians(0),radians(360),PIE);
//Bild speichern
save(timestamp()+"_"+bildname+"_Itten_Sunburst.png");

}


// timestamp
String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}