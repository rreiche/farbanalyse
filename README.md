# Digitale Ikonik

Skripte, mit denen die in meinem Aufsatz "Digitale Ikonik" erstellten Visualisierungen erzeugt wurden (siehe Ruth Reiche: "Digitale Ikonik", in: Philipp Hegel und Canan Hastik (Hg.): Bilddaten in den digitalen Geisteswissenschaften, 2019). 
Die Skripte sind nicht optimiert und erheben nicht den Anspruch, einen 'schönen' Code zu präsentieren. Sie werden an dieser Stelle dennoch veröffentlicht, um wissenschaftliche Transparenz zu gewährleisten.


Es handelt sich um drei Skripte:
1. Sortierung einer Pixelgrafik nach Farbwerten
2. Erstellung eines Kuchendiagrammes aus einem auf 12 Farben reduzierten Bild
3. Erstellung eines Sunburst-Diagrammes aus einem auf die 12 von Johannes Itten benannten Grundfarben reduzierten Bildes

Für 2. und 3. ist eine Vorverarbeitung notwendig. Mit jedem gängigen Bildprogramm lässt sich ein RGB-Bild über den indizierten Modus auf eine gewisse Anzahl von Farben reduzieren.
Ich empfehle Gimp, ein frei verfügbares und quelloffenes Bildbearbeitungsprogramm. Siehe: https://www.gimp.org/

Für das Ausführen der Skripte ist die Installation von Processing notwendig. Siehe hierzu: https://processing.org/

Ist Processing installiert, muss desweiteren Folgendes berücksichtigt werden:
- Der Code muss in einem Ordner gespeichert sein, der denselben Namen trägt wie die Sketch-Datei. 
- Die Ausgangsbilder müssen in diesem Ordner wiederum in einem Ordner namens "data" abgelegt sein.
- Der Code muss ggf. angepasst werden (z.B. Nennung des Dateinamens, Bildgröße). Der Code ist an den entsprechenden Stellen kommentiert.

Die Skripte nutzen die "Generative Gestaltung Library". Deshalb muss diese über den Processing Package Manager importiert werden, um die Skripte ausführen zu können. 
Wählen Sie hierzu "Add Library..." aus dem Untermenu "Import Library..." innerhalb des Sketchmenus. Siehe hierzu auch: http://www.generative-gestaltung.de/1/code#library

Viel Spaß beim Ausprobieren!