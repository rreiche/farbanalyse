// P_1_2_2_01.pde, modified by Ruth Reiche, 2018
// 
// Generative Gestaltung, ISBN: 978-3-87439-759-9
// First Edition, Hermann Schmidt, Mainz, 2009
// Hartmut Bohnacker, Benedikt Gross, Julia Laub, Claudius Lazzeroni
// Copyright 2009 Hartmut Bohnacker, Benedikt Gross, Julia Laub, Claudius Lazzeroni
//
// http://www.generative-gestaltung.de
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * extract and sort the color palette of an image

 * KEYS
 * 1                   : no color sorting
 * 2                   : sort colors on hue
 * 3                   : sort colors on saturation
 * 4                   : sort colors on brightness
 * 5                   : sort colors on grayscale (luminance)
 * s                   : save png
 * p                   : save pdf
 * c                   : save color palette
 */

import generativedesign.*;
import processing.pdf.*;
import java.util.Calendar;

boolean savePDF = false;

PImage img;
color[] colors;

String marker;
String sortMode = null;
String bildname;


void setup(){ 
  size(517, 640); //gleich der Größe des zu sortierenden Bildes, manuell anpassen!
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();
  //noCursor();
  img = loadImage("Macke_Original.jpg"); 
  bildname= "Macke_Original";
}


void draw(){
  if (savePDF) {
    beginRecord(PDF, timestamp()+".pdf");
    colorMode(HSB, 360, 100, 100, 100);
    noStroke();
  }

  
  int tileCount = width; //keine Reduktion, d.h. so viele Rechtecke wie Pixel
  float rectSize = width / float(tileCount);

  // get colors from image
  int i = 0; 
  colors = new color[tileCount*height];
  for (int gridY=0; gridY<height; gridY++) {
    for (int gridX=0; gridX<tileCount; gridX++) {
      int px = (int) (gridX * rectSize);
      int py = (int) (gridY * rectSize);
      colors[i] = img.get(px, py);
      i++;
    }
  }

  // sort colors
  if (sortMode != null) colors = GenerativeDesign.sortColors(this, colors, sortMode);
  

  // draw grid
  i = 0;
  for (int gridY=0; gridY<height; gridY++) {
    for (int gridX=0; gridX<tileCount; gridX++) {
      fill(colors[i]);
      rect(gridX*rectSize, gridY*rectSize, rectSize, rectSize);
      i++;
    }
  }

  if (savePDF) {
    savePDF = false;
    endRecord();
  }
}


void keyReleased(){
  if (key == '1') {sortMode = null;marker= "unsortiert";}
  if (key == '2') {sortMode = GenerativeDesign.HUE; marker = "hue";}
  if (key == '3') {sortMode = GenerativeDesign.SATURATION;marker="saturation";}
  if (key == '4') {sortMode = GenerativeDesign.BRIGHTNESS;marker = "brightness";}
  if (key=='s' || key=='S') saveFrame(timestamp()+"_"+bildname+"_"+marker+".jpg");
}


// timestamp
String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}