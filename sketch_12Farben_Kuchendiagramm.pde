//Dr. Ruth Reiche, 2018
//Das Skript erstellt ein Kuchendiagramm mit der Farbverteilung des geladenen Bildes. 
//Das Bild muss vorab auf 12 Farben reduziert werden (z.B. mit Gimp) und als png gespeichert sein.

import generativedesign.*;
import java.util.Calendar;

PImage img;
color[] colors;
color[] twelvecolors= new color[12];
int[] farbzaehler;
String bildname;



void setup(){ 
  
  size (600,600); //Leinwandgröße für das Kuchendiagramm
  background(255); //Hintergrundfarbe
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();
  noLoop();
  
  img = loadImage("Bildname.png"); //Bild laden - Dateiname muss exakt übereinstimmen!
  bildname ="Bildname"; // unter diesem Namen wird das generierte Bild gespeichert
  
}


void draw(){

// Farben aus dem Bild auslesen
int j = 0; 
colors = new color[img.width*img.height];
for (int gridY=0; gridY<img.height; gridY++) {
  for (int gridX=0; gridX<img.width; gridX++) {
    colors[j] = img.get(gridX, gridY);
    j++;
  }
}

println("Länge des Farbarrays: "+ colors.length);
println("Farbarray Position 1: "+colors[1]);

// sort colors
String sortMode = GenerativeDesign.HUE;
colors = GenerativeDesign.sortColors(this, colors, sortMode);
  
farbzaehler= new int[12];
println("Länge des Farbzaehlers: "+farbzaehler.length);
//farbzaehler[0]=5;
//farbzaehler[0]=farbzaehler[0]+1;
//println("Wert Farbzaehler Position 1: "+farbzaehler[0]);


int a=0;
while (a<colors.length){
  if (colors[a]==colors[0])  {
    farbzaehler[0]=farbzaehler[0]+1;
  }
  else if (colors[a]==colors[farbzaehler[0]])  {
    farbzaehler[1]=farbzaehler[1]+1;
    
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]])  {
  farbzaehler[2]=farbzaehler[2]+1;
  
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]])  {
  farbzaehler[3]=farbzaehler[3]+1;
  
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]])  {
  farbzaehler[4]=farbzaehler[4]+1;
  
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]])  {
  farbzaehler[5]=farbzaehler[5]+1;
  
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]])  {
  farbzaehler[6]=farbzaehler[6]+1;
  
 
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]])  {
  farbzaehler[7]=farbzaehler[7]+1;
  }
  
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]])  {
  farbzaehler[8]=farbzaehler[8]+1;
  
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]+farbzaehler[8]])  {
  farbzaehler[9]=farbzaehler[9]+1;
  
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]+farbzaehler[8]+farbzaehler[9]])  {
  farbzaehler[10]=farbzaehler[10]+1;
  }
  else if(colors[a]==colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]+farbzaehler[8]+farbzaehler[9]+farbzaehler[10]])  {
  farbzaehler[11]=farbzaehler[11]+1;
  
  }
a++;
}

twelvecolors[0]=colors[0];
twelvecolors[1]=colors[farbzaehler[0]];
twelvecolors[2]=colors[farbzaehler[0]+farbzaehler[1]];
twelvecolors[3]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]];
twelvecolors[4]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]];
twelvecolors[5]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]];
twelvecolors[6]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]];
twelvecolors[7]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]];
twelvecolors[8]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]];
twelvecolors[9]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]+farbzaehler[8]];
twelvecolors[10]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]+farbzaehler[8]+farbzaehler[9]];
twelvecolors[11]=colors[farbzaehler[0]+farbzaehler[1]+farbzaehler[2]+farbzaehler[3]+farbzaehler[4]+farbzaehler[5]+farbzaehler[6]+farbzaehler[7]+farbzaehler[8]+farbzaehler[9]+farbzaehler[10]];


println("Farbe 1: "+farbzaehler[0]);
println("Farbe 2: "+farbzaehler[1]);
println("Farbe 3: "+farbzaehler[2]);
println("Farbe 4: "+farbzaehler[3]);
println("Farbe 5: "+farbzaehler[4]);
println("Farbe 6: "+farbzaehler[5]);
println("Farbe 7: "+farbzaehler[6]);
println("Farbe 8: "+farbzaehler[7]);
println("Farbe 9: "+farbzaehler[8]);
println("Farbe 10: "+farbzaehler[9]);
println("Farbe 11: "+farbzaehler[10]);
println("Farbe 12: "+farbzaehler[11]);


//Prozentualen Anteil berechnen
float pixelanzahl=img.width*img.height;

float prozent_farbe1=farbzaehler[0]*100/pixelanzahl; 
float prozent_farbe2=farbzaehler[1]*100/pixelanzahl; 
float prozent_farbe3=farbzaehler[2]*100/pixelanzahl; 
float prozent_farbe4=farbzaehler[3]*100/pixelanzahl;
float prozent_farbe5=farbzaehler[4]*100/pixelanzahl; 
float prozent_farbe6=farbzaehler[5]*100/pixelanzahl; 
float prozent_farbe7=farbzaehler[6]*100/pixelanzahl; 
float prozent_farbe8=farbzaehler[7]*100/pixelanzahl; 
float prozent_farbe9=farbzaehler[8]*100/pixelanzahl; 
float prozent_farbe10=farbzaehler[9]*100/pixelanzahl; 
float prozent_farbe11=farbzaehler[10]*100/pixelanzahl; 
float prozent_farbe12=farbzaehler[11]*100/pixelanzahl; 


//Ausgabe in der Konsole (als Test)
println("Prozent Farbe 1: "+prozent_farbe1);
println("Prozent Farbe 2: "+prozent_farbe2);
println("Prozent Farbe 3: "+prozent_farbe3);
println("Prozent Farbe 4: "+prozent_farbe4);
println("Prozent Farbe 5: "+prozent_farbe5);
println("Prozent Farbe 6: "+prozent_farbe6);
println("Prozent Farbe 7: "+prozent_farbe7);
println("Prozent Farbe 8: "+prozent_farbe8);
println("Prozent Farbe 9: "+prozent_farbe9);
println("Prozent Farbe 10: "+prozent_farbe10);
println("Prozent Farbe 11: "+prozent_farbe11);
println("Prozent Farbe 12: "+prozent_farbe12);

//zeichne Kreis nach Itten

/*Parameter für Kreise: 
Mittelpunkt-x-Koordinate, 
Mittelpunkt-y-Koordinate, 
Durchmesser Breite, 
Durchmesser Höhe
start-Winkel,
End-Winkel, 
Modus
*/

int i=0;
int x = 300;
int y = 300;
int breite = 300;
int hoehe = 300;
//int start =255; //für Sunburst
//int ende = 285; //für Sunburst
float start =0; //für Wahl-O-Mat
float ende = 360; //für Wahl-O-Mat


//Fiktive Angaben (als Test)
//int[] prozentangaben= {10,5,20,5,4,10,6,7,30,3,3,7};

//Echte Angaben
float[] prozentangaben= {prozent_farbe1,prozent_farbe2,prozent_farbe3,prozent_farbe4,prozent_farbe5,prozent_farbe6,prozent_farbe7,prozent_farbe8,prozent_farbe9,prozent_farbe10,prozent_farbe11,prozent_farbe12};


while (i<12){
  fill(twelvecolors[i]);
  
  //Kuchendiagramm mit Prozentangeben, Sunburst-Charakter (Durchmesser ändern)
  /*arc(x,y,breite+prozentangaben[i]*3,hoehe+prozentangaben[i]*3,radians(start),radians(ende),PIE);
  i++;
  start =start+30;
  ende=ende+30;
  */
  
  //Kuchendiagramm mit Prozentangeben, Wahl-O-Mat-Charakter (Winkel ändern) 
  
  arc(x,y,breite+200,hoehe+200,radians(start),radians(ende),PIE);
  
  start =start+prozentangaben[i]*3.6;
  i++;
  
}


save(timestamp()+"_"+bildname+"_12Farben_Kuchendiagramm.png");
}


// timestamp
String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}